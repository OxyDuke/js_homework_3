
const age = prompt('Please, enter your age');

if (typeof(Number(age)) === 'number' && !isNaN(Number(age))) {
  if (age <= 12) {
    alert('You are a child');
  } else if (age <= 18) {
    alert('You are a teenager');
  } else {
    alert('You are grownup');
  }
} else {
  alert('You have entered not a number.');
} 


const month = prompt('Введіть місяць року (українською мовою маленькими літерами):');

switch (month) {
  case 'січень':
  case 'березень':
  case 'травень':
  case 'липень':
  case 'серпень':
  case 'жовтень':
  case 'грудень':
    console.log('У цьому місяці 31 день.');
    break;
  case 'квітень':
  case 'червень':
  case 'вересень':
  case 'листопад':
    console.log('У цьому місяці 30 днів.');
    break;
  case 'лютий':
    console.log('У цьому місяці 28 або 29 днів.');
    break;
  default:
    console.log('Ви ввели невірний місяць. Спробуйте ще раз!');
}